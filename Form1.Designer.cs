﻿namespace KRIZIC_KRUZIC_Csharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.static_whose_turn = new System.Windows.Forms.Label();
            this.who_plays = new System.Windows.Forms.Label();
            this.new_games = new System.Windows.Forms.Button();
            this.textBox_name1 = new System.Windows.Forms.TextBox();
            this.textBox_name2 = new System.Windows.Forms.TextBox();
            this.static_igrac1 = new System.Windows.Forms.Label();
            this.static_igrac2 = new System.Windows.Forms.Label();
            this.rezultat = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 300);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Mouse_click);
            // 
            // static_whose_turn
            // 
            this.static_whose_turn.AutoSize = true;
            this.static_whose_turn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.static_whose_turn.Location = new System.Drawing.Point(12, 356);
            this.static_whose_turn.Name = "static_whose_turn";
            this.static_whose_turn.Size = new System.Drawing.Size(152, 20);
            this.static_whose_turn.TabIndex = 1;
            this.static_whose_turn.Text = "Trenutno na potezu:";
            // 
            // who_plays
            // 
            this.who_plays.AutoSize = true;
            this.who_plays.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.who_plays.Location = new System.Drawing.Point(10, 380);
            this.who_plays.Name = "who_plays";
            this.who_plays.Size = new System.Drawing.Size(0, 31);
            this.who_plays.TabIndex = 2;
            // 
            // new_games
            // 
            this.new_games.Location = new System.Drawing.Point(95, 438);
            this.new_games.Name = "new_games";
            this.new_games.Size = new System.Drawing.Size(98, 26);
            this.new_games.TabIndex = 3;
            this.new_games.Text = "Nova igra";
            this.new_games.UseVisualStyleBackColor = true;
            this.new_games.Click += new System.EventHandler(this.new_games_Click);
            // 
            // textBox_name1
            // 
            this.textBox_name1.Location = new System.Drawing.Point(12, 323);
            this.textBox_name1.Name = "textBox_name1";
            this.textBox_name1.Size = new System.Drawing.Size(96, 20);
            this.textBox_name1.TabIndex = 4;
            // 
            // textBox_name2
            // 
            this.textBox_name2.Location = new System.Drawing.Point(182, 323);
            this.textBox_name2.Name = "textBox_name2";
            this.textBox_name2.Size = new System.Drawing.Size(96, 20);
            this.textBox_name2.TabIndex = 5;
            // 
            // static_igrac1
            // 
            this.static_igrac1.AutoSize = true;
            this.static_igrac1.Location = new System.Drawing.Point(40, 307);
            this.static_igrac1.Name = "static_igrac1";
            this.static_igrac1.Size = new System.Drawing.Size(43, 13);
            this.static_igrac1.TabIndex = 7;
            this.static_igrac1.Text = "Igrač 1:";
            // 
            // static_igrac2
            // 
            this.static_igrac2.AutoSize = true;
            this.static_igrac2.Location = new System.Drawing.Point(208, 307);
            this.static_igrac2.Name = "static_igrac2";
            this.static_igrac2.Size = new System.Drawing.Size(43, 13);
            this.static_igrac2.TabIndex = 8;
            this.static_igrac2.Text = "Igrač 2:";
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rezultat.Location = new System.Drawing.Point(114, 307);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(62, 39);
            this.rezultat.TabIndex = 9;
            this.rezultat.Text = "     ";
            this.rezultat.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 476);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.static_igrac2);
            this.Controls.Add(this.static_igrac1);
            this.Controls.Add(this.textBox_name2);
            this.Controls.Add(this.textBox_name1);
            this.Controls.Add(this.new_games);
            this.Controls.Add(this.who_plays);
            this.Controls.Add(this.static_whose_turn);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Iks Oks";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label static_whose_turn;
        private System.Windows.Forms.Label who_plays;
        private System.Windows.Forms.Button new_games;
        private System.Windows.Forms.TextBox textBox_name1;
        private System.Windows.Forms.TextBox textBox_name2;
        private System.Windows.Forms.Label static_igrac1;
        private System.Windows.Forms.Label static_igrac2;
        private System.Windows.Forms.Label rezultat;
    }
}

